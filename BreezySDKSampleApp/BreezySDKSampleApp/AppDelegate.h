//
//  AppDelegate.h
//  BreezySDKSampleApp
//
//  Created by Stanislav Razbegin on 9/10/15.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

