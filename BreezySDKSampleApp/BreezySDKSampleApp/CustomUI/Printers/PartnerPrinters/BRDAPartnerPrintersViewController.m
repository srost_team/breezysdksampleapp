//
//  BRDAPartnerPrintersViewController.m
//  Breezy-iOS2
//
//  Created by Alex Zimin on 27/09/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import "BRDAPartnerPrintersViewController.h"
#import "BreezySDK.h"
#import "BRPartnerAnnotation.h"

@import MapKit;

@interface BRDAPartnerPrintersViewController () <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic) NSArray *requestResult;

@end

@implementation BRDAPartnerPrintersViewController

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    CLLocation *location = (CLLocation *)self.mapView.userLocation;
    [self requestOffices:location withRadius:100000.0];
}

#pragma mark - request for offices

- (void)requestOffices:(CLLocation *)location withRadius:(float)radius
{
    typeof(self) weakSelf = self;
    
    [[BreezySDK sharedInstance] requestPartnerPrintersWithLocation:location radius:radius completion:^(BOOL success, NSArray *arrayOfPartnerPrinters) {
        if (success) {
            self.requestResult = arrayOfPartnerPrinters;
            [weakSelf addAnnotationsFromArray:arrayOfPartnerPrinters];
        }
    }];
}

#pragma mark - add annotations

- (void)addAnnotationsFromArray:(NSArray *)annotationArray
{
    NSMutableArray *mutablePinsArray = [NSMutableArray array];
    [self removeAllPinsExceptUserLocation];
    
    for (int i = 0; i < annotationArray.count; i++) {
        
        BRSDKPartnerPrinter *partnerPrinter = annotationArray[i];
        BRPrinterLocationEntity *printerLocation = partnerPrinter.printerLocation;
        
        CLLocationCoordinate2D location;
        location.latitude = [printerLocation.addressLatitude doubleValue];
        location.longitude = [printerLocation.addressLongitude doubleValue];
        
        BRPartnerAnnotation *pin = [[BRPartnerAnnotation alloc] init];
        pin.coordinate = location;
        pin.title = printerLocation.name;
        pin.printerLocation = printerLocation;
        
        [mutablePinsArray addObject:pin];
        
    }
    
    [self.mapView addAnnotations:mutablePinsArray];
    
}

- (void)removeAllPinsExceptUserLocation
{
    id userLocation = [self.mapView userLocation];
    NSMutableArray *pins = [[NSMutableArray alloc] initWithArray:[self.mapView annotations]];
    if ( userLocation != nil ) {
        [pins removeObject:userLocation];
    }
    
    [self.mapView removeAnnotations:pins];
}

#pragma mark - Map Delegate

- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if( [annotation isKindOfClass:[MKUserLocation class]] ) {
        return nil;
    }
    
    static NSString *identifier = @"myAnnotation";
    MKPinAnnotationView * annotationView = (MKPinAnnotationView*)[self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    
    if ( !annotationView ) {
        annotationView = [[MKPinAnnotationView alloc] init];
        annotationView.canShowCallout = YES;
        
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        [rightButton addTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
        annotationView.rightCalloutAccessoryView = rightButton;

    } else {
        annotationView.annotation = annotation;
    }
    
    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    return annotationView;
    
}

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation
{
    MKCoordinateSpan span = MKCoordinateSpanMake(0.5, 0.5);
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(aUserLocation.coordinate.latitude, aUserLocation.coordinate.longitude);
    MKCoordinateRegion region = MKCoordinateRegionMake(location, span);
    [aMapView setRegion:region animated:YES];
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    CLLocationCoordinate2D centerCoor = [self getCenterCoordinate];
    CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:centerCoor.latitude longitude:centerCoor.longitude];
    
    [self requestOffices:centerLocation withRadius:[self getRadius]];
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view
calloutAccessoryControlTapped:(UIControl *)control
{
    BRPartnerAnnotation *tempAnnotation = view.annotation;
    BRPrinterEntity *printer;
    
    for (BRSDKPartnerPrinter *partnerPrinters in self.requestResult) {
        if (partnerPrinters.printerLocation == tempAnnotation.printerLocation) {
            printer = partnerPrinters.printer;
            break;
        }
    }
    
    [BreezySDK sharedInstance].selectedPrinter = printer;
    [self.navigationController popToRootViewControllerAnimated:YES];
}


#pragma mark - radius

- (CLLocationCoordinate2D)getCenterCoordinate
{
    CLLocationCoordinate2D centerCoor = [self.mapView centerCoordinate];
    return centerCoor;
}

- (CLLocationCoordinate2D)getTopCenterCoordinate
{
    // to get coordinate from CGPoint of your map
    CLLocationCoordinate2D topCenterCoor = [self.mapView convertPoint:CGPointMake(self.mapView.frame.size.width / 2.0f, 0) toCoordinateFromView:self.mapView];
    return topCenterCoor;
}

- (CLLocationDistance)getRadius
{
    CLLocationCoordinate2D centerCoor = [self getCenterCoordinate];
    CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:centerCoor.latitude longitude:centerCoor.longitude];
    
    CLLocationCoordinate2D topCenterCoor = [self getTopCenterCoordinate];
    CLLocation *topCenterLocation = [[CLLocation alloc] initWithLatitude:topCenterCoor.latitude longitude:topCenterCoor.longitude];
    
    CLLocationDistance radius = [centerLocation distanceFromLocation:topCenterLocation];
    
    return radius;
}


@end
