//
//  BRDASelectPrinterViewController.m
//  Breezy-iOS2
//
//  Created by Alex Zimin on 26/09/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import "BRDASelectPrinterViewController.h"
#import "BRDAAlertView.h"

@interface BRDASelectPrinterViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSArray *printers;

@end

@implementation BRDASelectPrinterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Loading local printers for current presentation from cache data
    self.printers = [[BreezySDK sharedInstance] localPrintersWithType:self.type];
    
    // Load new printes asynchronously
    [[BreezySDK sharedInstance] loadPrintersWithType:self.type success:^(NSArray *printers) {
        self.printers = printers;
        [self.tableView reloadData];
    } failure:^(NSError *error) {
        [BRDAAlertView showError:error withMessage:@"Failed loading printers"];
    }];
}

#pragma mark - Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.printers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.textLabel.adjustsFontSizeToFitWidth = true;
    }
    
    BRPrinterEntity *printer = self.printers[indexPath.row];
    cell.textLabel.text = [printer.printerName stringByAppendingFormat:@" - %@", printer.printerId];
    
    return cell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [BreezySDK sharedInstance].selectedPrinter = self.printers[indexPath.row];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
