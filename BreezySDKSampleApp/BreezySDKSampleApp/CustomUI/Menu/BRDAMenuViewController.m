//
//  BRDAMenuViewController.m
//  Breezy-iOS2
//
//  Created by Alex Zimin on 26/09/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import "BRDAMenuViewController.h"
#import "BRDASelectPrinterTypeViewController.h"
#import "BRDAUploadingViewController.h"
#import "BreezySDK.h"
#import "BRDAAlertView.h"

@interface BRDAMenuViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) UIBarButtonItem *loginBarButtonItem;

@end

@implementation BRDAMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *dismissViewCotnrollerButton = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStyleDone target:self action:@selector(close:)];
    self.navigationItem.leftBarButtonItem = dismissViewCotnrollerButton;
    
    self.loginBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Log in" style:UIBarButtonItemStyleDone target:self action:@selector(login:)];
    self.navigationItem.rightBarButtonItem = self.loginBarButtonItem;
    
    [[BreezySDK sharedInstance] setLoginBaseURLPath: BR_SDK_LOGIN_BASE_URL oauthClientId: BR_SDK_OAUTH_CLIENT_ID oauthClientSecret: BR_SDK_OAUTH_CLIENT_SECRET];
}

- (void)viewWillAppear:(BOOL)animated {
    
    self.loginBarButtonItem.enabled = ![[BreezySDK sharedInstance] isLoggedIn];
    
    [self.tableView reloadData];
}

- (void)close:(UIBarButtonItem*)sender  {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)login:(UIBarButtonItem*)sender  {
    self.loginBarButtonItem.enabled = false;
    
    [[BreezySDK sharedInstance] loginFromViewController:self success:^{
        self.loginBarButtonItem.enabled = false;
    } failure:^(NSError *error) {
        self.loginBarButtonItem.enabled = true;
        [BRDAAlertView showError:error withMessage:@"Logining error"];
    }];
}

- (NSArray*)sectionsTitles {
    return @[@"Document", @"Printer", @"Printer settings", @"Upload"];
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ( [BreezySDK sharedInstance].isLoggedIn ) {
         return 4;
    } else {
        return 0;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.sectionsTitles[section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    if ( indexPath.section == 0 ) {
        cell.textLabel.text = [self documentCellTitle];
    } else if ( indexPath.section == 1 ) {
        cell.textLabel.text = [self printerCellTitle];
    } else if ( indexPath.section == 2) {
        cell.textLabel.text = [self printerSettingsCellTitle];
        
        BOOL isEnable = ([BreezySDK sharedInstance].selectedPrinter) ? true : false;
        
        cell.textLabel.alpha = isEnable ? 1.0 : 0.5;
        [cell setUserInteractionEnabled:isEnable];
        
    } else {
        cell.textLabel.text = @"Upload";
        cell.textLabel.alpha = ( [BreezySDK sharedInstance].isPrintingPermitted ) ? 1.0 : 0.5;
        [cell setUserInteractionEnabled:[BreezySDK sharedInstance].isPrintingPermitted];
    }
    
    return cell;
}

#pragma mark - Cells

- (NSString*)documentCellTitle {
    if ( [BreezySDK sharedInstance].currentDocument ) {
        return [BreezySDK sharedInstance].currentDocument.friendlyName;
    } else {
        return @"Tap to select document";
    }
}

- (NSString*)printerCellTitle {
    if ( [BreezySDK sharedInstance].selectedPrinter ) {
        return [BreezySDK sharedInstance].selectedPrinter.printerName;
    } else {
        return @"Tap to select printer";
    }
}

- (NSString*)printerSettingsCellTitle {
    if ( ![BreezySDK sharedInstance].selectedPrinter ) {
        return @"Select printer first";
    }
    
    if ( [BreezySDK sharedInstance].currentPrinterSettings ) {
        return @"Printer settings loaded";
    } else {
        return @"Tap to load printer settings";
    }
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        [self addDocumentData];
    } else if (indexPath.section == 1) {
        [self selectPrinters];
    } else if (indexPath.section == 2) {
        [self loadPrinterSettings];
    } else if (indexPath.section == 3) {
        [self printDocument];
    }
}

#pragma mark - Actions

- (void)addDocumentData
{
    NSData *testData = [@"Data" dataUsingEncoding:NSUTF8StringEncoding];
    [[BreezySDK sharedInstance] saveData:testData withFileName:@"Test document" fileExtension:@"data" success:^{
        NSLog(@"Document saved");
    } failure:^(NSError *error) {
        [BRDAAlertView showError:error withMessage:@"Error saving document data"];
    }];
    [self.tableView reloadData];
}

- (void)selectPrinters
{
    BRDASelectPrinterTypeViewController *selectPrintersVC = [[BRDASelectPrinterTypeViewController alloc] initWithNibName:@"BRDASelectPrinterTypeViewController" bundle:nil];
    [self.navigationController pushViewController:selectPrintersVC animated:YES];
}

- (void)loadPrinterSettings
{
    if ( [BreezySDK sharedInstance].currentPrinterSettings ) {
        [self.tableView reloadData];
    } else {
        [[BreezySDK sharedInstance] loadPrinterSettingsForCurrentPrinterWithSuccess:^(BRPrinterSettingsEntity *printerSettings) {
            [self.tableView reloadData];
        } failure:^(NSError *error) {
            [BRDAAlertView showError:error withMessage:@"Error loading printer settings"];
        }];
    }
}

- (void)printDocument
{
    BRDAUploadingViewController *selectPrintersVC = [[BRDAUploadingViewController alloc] initWithNibName:@"BRDAUploadingViewController" bundle:nil];
    [self.navigationController pushViewController:selectPrintersVC animated:YES];
}


@end
