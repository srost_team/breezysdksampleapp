//
//  BRDAUploadingViewController.m
//  Breezy-iOS2
//
//  Created by Alex Zimin on 26/09/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import "BRDAUploadingViewController.h"
#import "BreezySDK.h"
#import "BRDAAlertView.h"

@interface BRDAUploadingViewController () <BreezySDKUploadDelegate>

@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (nonatomic) BOOL isUploading;

@end

@implementation BRDAUploadingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)actionButtonAction:(UIButton *)sender {
    
    if ( self.isUploading ) {
        [[BreezySDK sharedInstance] stopPrinting];
        return;
    }
    
    NSError *error = nil;
    BOOL success = [[BreezySDK sharedInstance] printWithDelegate:self statusBlock:^(EBRSDKUploadStatus status) {
        NSLog(@"Status block notifies that status change to: %@", [self statusToString:status]);
    } error:&error];
    
    if ( !success ) {
        [BRDAAlertView showError:error withMessage:@"Document upload error"];
    }
}

- (void)setIsUploading:(BOOL)isUploading {
    _isUploading = isUploading;
    
    if ( _isUploading ) {
        [self.actionButton setTitle:@"Stop" forState:UIControlStateNormal];
    } else {
        [self.actionButton setTitle:@"Start" forState:UIControlStateNormal];
    }
}

#pragma mark - BreezySDKUploadDelegate

- (void)willStartUpload {
    self.statusLabel.text = @"Uploading starts";
    self.isUploading = true;
}

- (void)didChangeUploadStatus: (EBRSDKUploadStatus)status {
    self.statusLabel.text = [self statusToString:status];
}

- (void)didUploadDocument {
    self.isUploading = false;
    [BRDAAlertView showAlertWithMessage:@"Document uploaded"];
}

- (void)uploadFailedWithError:(NSError*)error {
    self.isUploading = false;
    [BRDAAlertView showError:error withMessage:@"Document upload error"];
}

#pragma mark - Helper Methods

- (NSDictionary*)statusDictionary {
    return @{@(eBRSDKUploadStatusPreparing): @"Preparing",
             @(eBRSDKUploadStatusReleasing): @"Releasing",
             @(eBRSDKUploadStatusReleased): @"Released",
             @(eBRSDKUploadStatusUploading): @"Uploading",
             @(eBRSDKUploadStatusUploaded): @"Uploaded",
             @(eBRSDKUploadStatusUploadFailed): @"Upload Failed",
             @(eBRSDKUploadStatusFinalizing): @"Finalizing",
             @(eBRSDKUploadStatusFinished): @"Finished",
             @(eBRSDKUploadStatusFailed): @"Failed"};
}

- (NSString*)statusToString:(EBRSDKUploadStatus)status {
    NSString *stringValue = self.statusDictionary[@(status)];
    
    if ( !stringValue ) {
        return @"Unknown";
    }
    
    return stringValue;
}

@end
