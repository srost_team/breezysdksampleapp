//
//  main.m
//  BreezySDKSampleApp
//
//  Created by Stanislav Razbegin on 9/10/15.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
