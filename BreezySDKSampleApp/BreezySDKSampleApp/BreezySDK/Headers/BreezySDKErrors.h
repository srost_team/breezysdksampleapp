//
//  BreezySDKErrors.h
//  Breezy-iOS2
//
//  Created by Kostiantyn Sokolinskyi on 2/26/15.
//  Copyright (c) 2015 BreezyPrint Corporation. All rights reserved.
//

#ifndef Breezy_iOS2_BreezySDKErrors_h
#define Breezy_iOS2_BreezySDKErrors_h

extern NSString * const BRSDKErrorDomain;
typedef NS_ENUM(NSInteger, BRSDKErrorType) {
    
    BRSDKGeneralNetworkError = 1,
    BRSDKAuthNetworkError,
    BRSDKUploadNetworkError,
    
    BRSDKDocumentNotSet = 1000,
    BRSDKPrinterNotSet,
    BRSDKRestrictedByPermissions,
    BRSDKPrinterSettingsNotSet,
    BRSDKFailedLoadingPrinters,
    BRSDKNoParentViewController,
    BRSDKUserAlreadyLoggedIn,
};

#endif
