//
//  BreezySDKDelegate.h
//  Breezy-iOS2
//
//  Created by Alex Zimin on 16/09/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BreezySDKUploadStatus.h"
#import "BRPrinterEntity.h"
#import "BRDocumentEntity.h"

@protocol BreezySDKDelegate <NSObject>

/*! Tells the delegate that user logged into Breezy */
- (void)didLogin;

/*! Notifies delegate about an error that occurred while saving document into Breezy storage
 * \param error error occurred during saving
 */
- (void)savingDocumentFailedWithError:(NSError*)error;

/*! Notifies delegate about an error that occurred inside the SDK
 * \param error error occurred
 */
- (void)sdkFailedWithError:(NSError*)error;

/*! Tells the delegate that user selected a particular printer
 * \param printer current printer
 */
-(void)didSelectPrinter:(BRPrinterEntity*)printer;

/*! Tells the delegate that document upload has finished
 * \param success upload status
 */
- (void)didUploadDocument: (BOOL)success;

@end




@protocol BreezySDKUploadDelegate <NSObject>

/*! Tells the delegate that document upload is about to start */
- (void)willStartUpload;

/*! Tells the delegate that document upload status has changed
 * \param status upload status
 */
- (void)didChangeUploadStatus: (EBRSDKUploadStatus)status;

/*! Tells the delegate that document uploaded to Breezy server */
- (void)didUploadDocument;

/*! Tells the delegate that document failed uploading to Breezy server
 * \param error error occurred during upload
 */
- (void)uploadFailedWithError:(NSError*)error;

@end
