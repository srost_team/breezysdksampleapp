//
//  BRPrinterSettingsEntity.h
//  Breezy-iOS2
//
//  Created by Alex Zimin on 15/12/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BRPrintJobEntity;

@interface BRPrinterSettingsEntity : NSManagedObject

@property (nonatomic) int16_t color;
@property (nonatomic) int16_t duplexing;
@property (nonatomic) int64_t numberOfCopies;
@property (nonatomic) int16_t orientation;
@property (nonatomic, retain) NSString * pageRange;
@property (nonatomic) BOOL printCoverPage;
@property (nonatomic, retain) BRPrintJobEntity *printJob;

@end
