//
//  BreezySDKLogLevels.h
//  Breezy-iOS2
//
//  Created by Kostiantyn Sokolinskyi on 2/26/15.
//  Copyright (c) 2015 BreezyPrint Corporation. All rights reserved.
//

#ifndef Breezy_iOS2_BreezySDKLogLevels_h
#define Breezy_iOS2_BreezySDKLogLevels_h

typedef NS_ENUM(NSUInteger, BRSDKLogLevel) {
    BRSDKLogLevelNone = 0,
    BRSDKLogLevelError = 1 << 0,
    BRSDKLogLevelWarn = 1 << 1,
    BRSDKLogLevelInfo = 1 << 2,
    BRSDKLogLevelDebug = 1 << 3,
    BRSDKLogLevelVerbose = 1 << 4,
    BRSDKLogLevelAll = BRSDKLogLevelError | BRSDKLogLevelWarn | BRSDKLogLevelInfo | BRSDKLogLevelDebug | BRSDKLogLevelVerbose
};


#endif
