//
//  BreezySDKPrinterSources.h
//  Breezy-iOS2
//
//  Created by Kostiantyn Sokolinskyi on 2/26/15.
//  Copyright (c) 2015 BreezyPrint Corporation. All rights reserved.
//

#ifndef Breezy_iOS2_BreezySDKPrinterSources_h
#define Breezy_iOS2_BreezySDKPrinterSources_h

typedef NS_ENUM(NSInteger, BRSDKPrinterSource) {
    BRSDKHomePrintersSource = 0,
    BRSDKOfficePrintersSource = 1,
    BRSDKPartnerPrintersSource = 2,
};

#endif
