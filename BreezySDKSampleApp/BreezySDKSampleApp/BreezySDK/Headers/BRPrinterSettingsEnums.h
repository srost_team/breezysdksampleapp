//
//  BRPrinterSettingsEnums.h
//  Breezy-iOS2
//
//  Created by Alex Zimin on 09/12/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(int64_t, EBRPrintColor) {
    eBRPrintColorUnknown = 0,
    eBRPrintColorRGB = 1,
    eBRPrintColorGrayscale = 2,
    eBRPrintColorMonochrome = 3
};

typedef NS_ENUM(int64_t, EBRPrintOrientation) {
    eBRPrintOrientationUnknown = 0,
    eBRPrintOrientationLandscape = 1,
    eBRPrintOrientationPortrait = 2,
    eBRPrintOrientationReverseLandscape = 3,
    eBRPrintOrientationReversePortrait = 4
};

typedef NS_ENUM(int64_t, EBRPrintDuplexing) {
    eBRPrintDuplexingUnknown = 0,
    eBRPrintDuplexingOneSide = 1,
    eBRPrintDuplexingTwoSidedShortEdge = 2,
    eBRPrintDuplexingTwoSidedLongEdge = 3
};