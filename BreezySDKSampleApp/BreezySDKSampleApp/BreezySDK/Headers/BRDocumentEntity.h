//
//  BRDocumentEntity.h
//  Breezy-iOS2
//
//  Created by Alex Zimin on 11/02/15.
//  Copyright (c) 2015 BreezyPrint Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BRFaxJobEntity, BRHistoryEntity, BRPrintJobEntity;

@interface BRDocumentEntity : NSManagedObject

@property (nonatomic) NSDate* createdAt;
@property (nonatomic, retain) NSString * docType;
@property (nonatomic, retain) NSString * externalUrl;
@property (nonatomic, retain) NSString * fileExtension;
@property (nonatomic, retain) NSString * filePath;
@property (nonatomic) int64_t fileSize;
@property (nonatomic, retain) NSString * friendlyName;
@property (nonatomic) int16_t source;
@property (nonatomic, retain) BRFaxJobEntity *faxJob;
@property (nonatomic, retain) BRHistoryEntity *historyId;
@property (nonatomic, retain) NSSet *printjob;
@property (nonatomic, retain) BRPrintJobEntity *printJob;
@end

@interface BRDocumentEntity (CoreDataGeneratedAccessors)

- (void)addPrintjobObject:(BRPrintJobEntity *)value;
- (void)removePrintjobObject:(BRPrintJobEntity *)value;
- (void)addPrintjob:(NSSet *)values;
- (void)removePrintjob:(NSSet *)values;

@end
