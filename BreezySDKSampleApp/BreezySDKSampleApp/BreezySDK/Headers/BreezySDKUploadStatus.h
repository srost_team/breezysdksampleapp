//
//  BreezySDKUploadStatus.h
//  Breezy-iOS2
//
//  Created by Kostiantyn Sokolinskyi on 3/13/15.
//  Copyright (c) 2015 BreezyPrint Corporation. All rights reserved.
//

#ifndef Breezy_iOS2_BreezySDKUploadStatus_h
#define Breezy_iOS2_BreezySDKUploadStatus_h

typedef NS_ENUM(NSInteger, EBRSDKUploadStatus) {
    eBRSDKUploadStatusPreparing = 0,
    eBRSDKUploadStatusReleasing,
    eBRSDKUploadStatusReleased,
    eBRSDKUploadStatusUploading,
    eBRSDKUploadStatusUploaded,
    eBRSDKUploadStatusUploadFailed,
    eBRSDKUploadStatusFinalizing,
    eBRSDKUploadStatusFinished,
    eBRSDKUploadStatusFailed
};

#endif
