//
//  BreezySDK.h
//  Breezy-iOS2
//
//  Created by Alex Zimin on 16/09/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BreezySDKDelegate.h"
#import "BRSDKPartnerPrinter.h"
#import "BRPrinterSettingsEntity.h"
#import "BRPrinterSettingsEnums.h"

#import "BreezySDKLogLevels.h"
#import "BreezySDKErrors.h"
#import "BreezySDKPrinterSources.h"

#define BR_SDK_LOGIN_BASE_URL @"http://api-v20-staging.breezy.com/"
#define BR_SDK_OAUTH_CLIENT_ID @"DP-GqheU93sSMKfG2Ao0rFhaOHcxAmSQLPoMclbhjikip1RNXsZFN1pLxNYwQmPDtMz"
#define BR_SDK_OAUTH_CLIENT_SECRET @"xr49R3XH2BkzBrv0vVTcFPazjymhTbWsfx22zaqGe9Li7yyZM8bJMPAA4OUlzMLX"

#define BR_SSO_KEY @"erez5aj5d9u4a7i5v7tqam47fxtwzdpdfgn7tvprsjyzvf49ozhy9a1wz8e78wl21dvc9qwh42k0gxqu54oxf74uzw3fcogf8e4t3nx6hrpu4d3kg7hkaysc9oqsoe7p"

@import MapKit;

/*! NSNotification parametr contains NSNumber - printer type */
extern NSString *kBRSDKNotificationPrintersDidUpdate;

@interface BreezySDK : NSObject

#pragma mark - SDK Setup

+ (BreezySDK*)sharedInstance;

/*! Adjusts Breezy log level so Breezy debug/error output can be seen in console 
 check BreezySDKLogLevels.h for levels
 */
+ (void)setLogLevel:(BRSDKLogLevel)logLevel;


#pragma mark - Authentication

@property (nonatomic, readonly) BOOL isLoggedIn;

/*! Logs out user and clean local storage */
- (void)logout;

/*! OAuth params must be set before authorising the user */
- (void)setLoginBaseURLPath: (NSString*)loginBaseURLPath oauthClientId: (NSString*)oauthClientId oauthClientSecret: (NSString*)oauthClientSecret;



#pragma mark - Delegate

@property (nonatomic,strong) id<BreezySDKDelegate> delegate;


#pragma mark - Breezy UI Methods

/*! Prints chosen document from assigned view controller.
 If user not loged in, thet SDK will present login controller automatically */
- (BOOL)printWithBreezyUIFromViewController: (UIViewController*)parentViewController
                                      error: (NSError**)error;

/*! dissmis current SDK controller */
- (void)dissmissBreezyUI;

#pragma mark - Document

/*! Naturally gives access to current document passed into the SDK */
@property (nonatomic, readonly) BRDocumentEntity *currentDocument;

/*! The following methods allow passing document for printing */

- (void)saveData: (NSData*)data
    withFileName: (NSString*)fileName
   fileExtension: (NSString*)fileExtension
         success: (void (^)())successBlock
         failure: (void (^)(NSError* error))failureBlock;

- (void)saveFileFromPath: (NSString*)filePath
                 success: (void (^)())successBlock
                 failure: (void (^)(NSError* error))failureBlock;

- (void)saveFileFromURL: (NSURL*)fileURL
                success: (void (^)())successBlock
                failure: (void (^)(NSError* error))failureBlock;

/*! Removes the document passed into the SDK */
- (void)removeDocument;


/*--------------------------------------------------------------------------------------------------*/
/*--------------- The following methods to be used in case of Custom UI implementation -------------*/
/*--------------------------------------------------------------------------------------------------*/

#pragma mark - Printers

/*! You can check available Printer Sources in BreezySDKPrinterSources.h */

/*! Property shall be used for setting printer intended as printing target */
@property (nonatomic) BRPrinterEntity *selectedPrinter;

/*! Upon success of this method, printers loaded from the server will end up in the local storage. They can be accessed later using localPrintersWithType: method.
    Loaded printers will also be returned as NSArray *printers array in success block.
 */
- (void)loadPrintersWithType: (BRSDKPrinterSource)type
                     success: (void (^)(NSArray *printers))success
                     failure: (void (^)(NSError* error))failure;

/*! Returns printers stored in local storage */
- (NSArray*)localPrintersWithType: (BRSDKPrinterSource)type;


/*! Loads partner printers
    NSArray *arrayOfPartnerPrinters in completion block contains BRSDKPartnerPrinter objects
 */
- (void)requestPartnerPrintersWithLocation: (CLLocation *)location
                                    radius: (CGFloat)radius
                                completion: (void (^)(BOOL success, NSArray *arrayOfPartnerPrinters))completion;


#pragma mark - Printer Settings

/*! Property shall be used for setting printer settings intended to be used when printing currentDocument on selectedPrinter */
@property (nonatomic) BRPrinterSettingsEntity *currentPrinterSettings;

/*! Loads printer settings from Breezy server. 
 
    You need to load printer settings, modify, if needed, and set currentPrinterSettings before printing currentDocument.
    Before printing, settings will be uploaded back to Breezy and stored. Next time you request printer settings, you will get ones stored during last print job.
 */
- (void)loadPrinterSettingsForCurrentPrinterWithSuccess: (void (^)(BRPrinterSettingsEntity *printerSettings))success
                                                failure: (void (^)(NSError* error))failure;


#pragma mark - Custom UI Login

/*! Raises Breezy Login View Controller from view controller passed into the method.
    If you use default Breezy SDL UI you can use printWithBreezyUIFromViewController:error: that will show login controller automatically */
- (void)loginFromViewController: (UIViewController*)parentViewController
                        success: (void (^)())success
                        failure: (void (^)(NSError* error))failure;

#pragma mark - API login

- (void)loginWithEmail: (NSString*)email password: (NSString*)password
               success: (void (^)())success
               failure: (void (^)(NSError *error))failure;

- (void)loginSSOWithEmail: (NSString*)email userGroups: (NSArray*)groups authKey: (NSString*)key
                  success: (void (^)())success
                  failure: (void (^)(NSError *error))failure;

#pragma mark - Printing

@property (nonatomic,strong) id<BreezySDKUploadDelegate> uploadDelegate;

/*! Property shows whether printing is enabled for the user based on availibility of the document, printer and current permissions loaded from Breezy system. It can be used to enable/disable printing button in the client code */
@property (nonatomic, readonly) BOOL isPrintingPermitted; 


/*! Uploads the document to Breezy system and sends it to printing.
 
    uploadDelegate along along with statusBlock will receive updates on every change in the print job status.
    Check BreezySDKUploadStatus.h to see all statuses
 */

- (BOOL)printWithDelegate: (id<BreezySDKUploadDelegate>)uploadDelegate
              statusBlock: (void (^)(EBRSDKUploadStatus status))statusBlock
                    error: (NSError**)error;

/*! Naturally stops uploading and printing process.
    Depending on how far in the process you are, it might not cancel the actual print job */
- (void)stopPrinting;

@end
