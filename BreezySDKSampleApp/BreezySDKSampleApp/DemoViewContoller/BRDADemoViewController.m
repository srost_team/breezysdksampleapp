//
//  BRSDKDemoViewController.m
//  Breezy-iOS2
//
//  Created by Alex Zimin on 26/09/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import "BRDALoginViewController.h"
#import "BRDADemoViewController.h"
#import "BRDADefaultUIDemoViewController.h"
#import "BRDAMenuViewController.h"
#import "BreezySDK.h"
//#import "CoreData+MagicalRecord.h"

@interface BRDADemoViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) UIBarButtonItem *logoutBarButtonItem;

@end

@implementation BRDADemoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.logoutBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Log out" style:UIBarButtonItemStyleDone target:self action:@selector(logout:)];
    self.navigationItem.rightBarButtonItem = self.logoutBarButtonItem;
}

- (void)viewWillAppear:(BOOL)animated {
    // If user logged in, then show log out button
    self.logoutBarButtonItem.enabled = [[BreezySDK sharedInstance] isLoggedIn];
}

- (void)logout:(UIBarButtonItem*)sender {
    [[BreezySDK sharedInstance] logout];
    self.logoutBarButtonItem.enabled = false;
}

#pragma mark - Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *title;
    if (indexPath.row == 0) {
        title  = @"Default UI with Default Login Screen";
    }
    else if (indexPath.row == 1) {
        title  = @"Default UI with Custom Login Screen";
    }
    else if (indexPath.row == 2) {
        title  = @"Custom UI with Custom Login Screen";
    }
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    cell.textLabel.text = title;
    
    return cell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        // Default UI, default login
        BRDADefaultUIDemoViewController *demoVC = [[BRDADefaultUIDemoViewController alloc] initWithNibName:@"BRDADefaultUIDemoViewController" bundle:nil];
        demoVC.isDefaultLogin = YES;
        UINavigationController *demoVCNav = [[UINavigationController alloc] initWithRootViewController: demoVC];
        [self presentViewController:demoVCNav animated:YES completion:nil];
    }
    else if (indexPath.row == 1) {
        // Default UI, custom login
        BRDADefaultUIDemoViewController *demoVC = [[BRDADefaultUIDemoViewController alloc] initWithNibName:@"BRDADefaultUIDemoViewController" bundle:nil];
        demoVC.isDefaultLogin = NO;
        UINavigationController *demoVCNav = [[UINavigationController alloc] initWithRootViewController: demoVC];
        [self presentViewController:demoVCNav animated:YES completion:nil];
    }
    else if (indexPath.row == 2){
        // Menu of custom UI and custom login
        if ([BreezySDK sharedInstance].isLoggedIn) {
            BRDAMenuViewController *demoVC = [[BRDAMenuViewController alloc] initWithNibName:@"BRDAMenuViewController" bundle:nil];
            UINavigationController *demoVCNav = [[UINavigationController alloc] initWithRootViewController: demoVC];
            [self presentViewController:demoVCNav animated:YES completion:nil];
        }
        else {
            BRDALoginViewController *loginVC = [[BRDALoginViewController alloc] initWithNibName:@"BRDALoginViewController" bundle:nil];
            UINavigationController *loginVCNav = [[UINavigationController alloc] initWithRootViewController: loginVC];
            
            if (IS_IPAD) {
                loginVCNav.modalPresentationStyle = UIModalPresentationFormSheet;
            }
            [self presentViewController:loginVCNav animated:YES completion:nil];
        }

    }
}

@end
