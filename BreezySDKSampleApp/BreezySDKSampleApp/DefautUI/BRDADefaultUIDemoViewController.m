//
//  BRSDKDemoViewController.m
//  Breezy-iOS2
//
//  Created by Alex Zimin on 18/09/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import "BRDADefaultUIDemoViewController.h"
#import "BreezySDK.h"
#import "BRDAAlertView.h"
#import "BRDALoginViewController.h"

#define LOG_ASYNC_ENABLED YES
#define BR_ENABLE_ACTIVE_RECORD_LOGGING 0

@import MapKit;

@interface BRDADefaultUIDemoViewController () <UITextFieldDelegate, BreezySDKDelegate, BreezySDKUploadDelegate>

@property (weak, nonatomic) IBOutlet UITextField *documentNameTextField;

@end

@implementation BRDADefaultUIDemoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [BreezySDK setLogLevel: BRSDKLogLevelError];
    [[BreezySDK sharedInstance] setLoginBaseURLPath: BR_SDK_LOGIN_BASE_URL oauthClientId: BR_SDK_OAUTH_CLIENT_ID oauthClientSecret: BR_SDK_OAUTH_CLIENT_SECRET];
    [BreezySDK sharedInstance].delegate = self;
    
    
    UIBarButtonItem *dismissViewCotnrollerButton = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStyleDone target:self action:@selector(close)];
    self.navigationItem.leftBarButtonItem = dismissViewCotnrollerButton;
}

- (void)close {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)print {
    [self createTestDocument];
    
    // Print from current controller
    NSError *error;
    BOOL success = [[BreezySDK sharedInstance] printWithBreezyUIFromViewController:self error:&error];
    
    if ( !success ) {
        [BRDAAlertView showError:error withMessage:@"Printing from current view controller error"];
    }
}

- (IBAction)printDocumentAction:(UIButton *)sender {
    if ([BreezySDK sharedInstance].isLoggedIn) {
        [self print];
    }
    else {
        if (self.isDefaultLogin) {
            [self print];
        }
        else {
            BRDALoginViewController *loginVC = [[BRDALoginViewController alloc] initWithNibName:@"BRDALoginViewController" bundle:nil];
            UINavigationController *loginVCNav = [[UINavigationController alloc] initWithRootViewController: loginVC];
            if (IS_IPAD) {
                loginVCNav.modalPresentationStyle = UIModalPresentationFormSheet;
            }
            [self presentViewController:loginVCNav animated:YES completion:nil];
        }
    }

}

- (void)createTestDocument {
    if ( self.documentNameTextField.text.length > 0 ) {
        NSData *testData = [@"Data" dataUsingEncoding:NSUTF8StringEncoding];
        
        [[BreezySDK sharedInstance] saveData:testData withFileName:self.documentNameTextField.text fileExtension:@"data" success:^{
            NSLog(@"Success saving doc");
        } failure:^(NSError *error) {
            [BRDAAlertView showError:error withMessage:@"Saving document data error"];
        }];
    }
}

#pragma mark - Text Field Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return true;
}

#pragma mark - SDK Basic Interface

/*! Method calling, when user was loging */
- (void)didLogin {
    NSLog(@"User did login");
}

/*! Method calling, when sdk requested document for save. Use SDKMethods to create document in this delegate. */
- (void)savingDocumentFailedWithError:(NSError *)error {
    NSLog(@"SDK requested document. Please implement [[BreezySDK sharedInstance] save...] in didRequestDocument method");
    [BRDAAlertView showError:error withMessage:@"Saving document failde with error"];
}

/*! Method calling, when sdk has some problems with requered data */
- (void)sdkFailedWithError:(NSError*)error {
    [BRDAAlertView showError:error withMessage:@"SDK error"];
}

/*! Method calling, when user select any printer
 * \param printer current printer
 */
- (void)didSelectPrinter:(BRPrinterEntity*)printer {
    NSLog(@"User did select printer: \nName: %@\nId: %@", printer.printerName, printer.printerId);
}

/*! Mathod calling, when document was uploaded
 * \param successful is printing was successful or not
 */
- (void)didUploadDocument:(BOOL)success {
    if ( success ) {
        NSLog(@"Document was uploaded");
    } else {
        NSLog(@"Document upload failed");
    }
    
}

#pragma mark - Uploading Delegat

/*! Method calling, when document uploading will starts */
- (void)willStartUpload {
    NSLog(@"Start upload");
}

/*! Method calling, when uploading status changed
 * \param status uploading status
 */
- (void)didChangeUploadStatus: (EBRSDKUploadStatus)status {
    NSLog(@"Change to status %li", status);
}

/*! Mathod calling, when document was uploaded */
- (void)didUploadDocument {
    NSLog(@"Did upload document");
}

/*! Mathod calling, when document upload failed with error
 * \param error error caused during uploading
 */
- (void)uploadFailedWithError:(NSError*)error {
    NSLog(@"Failed with error: %@", error);
}

@end
