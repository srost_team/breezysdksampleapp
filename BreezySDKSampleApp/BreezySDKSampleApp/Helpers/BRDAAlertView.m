//
//  BRDAAlertView.m
//  Breezy-iOS2
//
//  Created by Alex Zimin on 09/03/15.
//  Copyright (c) 2015 BreezyPrint Corporation. All rights reserved.
//

#import "BRDAAlertView.h"

@implementation BRDAAlertView

+ (void)showAlertWithMessage: (NSString*)message{
    [self showAlertWithMessage:message andTitle:nil];
}

+ (void)showAlertWithMessage: (NSString*)message
                    andTitle: (NSString*)title; {
#ifdef BRDA_ENABLE_ALERTS
    
    [[[UIAlertView alloc] initWithTitle: title
                                message: message
                               delegate: nil
                      cancelButtonTitle: @"Ok"
                      otherButtonTitles: nil] show];
    
#endif
}

+ (void)showError: (NSError*)error {
    [self showError:error withMessage:nil];
}

+ (void)showError: (NSError*)error
      withMessage: (NSString*)message {

#ifdef BRDA_ENABLE_ALERTS
    
    [[[UIAlertView alloc] initWithTitle: (message && message.length > 0) ? message : @"Error"
                                message: error.localizedDescription
                               delegate: nil
                      cancelButtonTitle: @"Ok"
                      otherButtonTitles: nil] show];
    
#endif
    
    if (message && message.length > 0) {
        NSLog(@"%@: %@", message, error.localizedDescription);
    } else {
        NSLog(@"%@", error.localizedDescription);
    }
    
    
}


@end
