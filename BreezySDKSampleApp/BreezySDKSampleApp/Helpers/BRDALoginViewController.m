//
//  BRDALoginViewController.m
//  BreezySDKSampleApp
//
//  Created by Stanislav Razbegin on 9/23/15.
//  Copyright (c) 2015 Stanislav Razbegin. All rights reserved.
//

#import "BRDALoginViewController.h"
#import "BRDAAlertView.h"

@interface BRDALoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *ssoEmailTextField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
- (IBAction)loginClick:(id)sender;

@end

@implementation BRDALoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.activityIndicator.hidden = YES;
    [BreezySDK setLogLevel: BRSDKLogLevelError];
    [[BreezySDK sharedInstance] setLoginBaseURLPath: BR_SDK_LOGIN_BASE_URL oauthClientId: BR_SDK_OAUTH_CLIENT_ID oauthClientSecret: BR_SDK_OAUTH_CLIENT_SECRET];
    [BreezySDK sharedInstance].delegate = self;
    UIBarButtonItem *dismissViewCotnrollerButton = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStyleDone target:self action:@selector(close)];
    self.navigationItem.leftBarButtonItem = dismissViewCotnrollerButton;
}

- (void)close {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)loginClick:(id)sender {
    if (self.ssoEmailTextField.text.length > 0) {
        [self startActivity];
        [self closeKeyboard];
        [[BreezySDK sharedInstance] loginSSOWithEmail:self.ssoEmailTextField.text userGroups:@[] authKey:BR_SSO_KEY success:^{
            [self finishAuthorizationProcessWithSuccess:YES];
        } failure:^(NSError *error) {
            [self finishAuthorizationProcessWithSuccess:NO];
        }];
    }
    else if (self.emailTextField.text.length > 0 && self.passwordTextField.text.length > 0) {
        [self startActivity];
        [self closeKeyboard];
        [[BreezySDK sharedInstance] loginWithEmail:self.emailTextField.text password:self.passwordTextField.text success:^{
            [self finishAuthorizationProcessWithSuccess:YES];
        } failure:^(NSError *error) {
            [self finishAuthorizationProcessWithSuccess:NO];
        }];
    }
    else {
        [BRDAAlertView showAlertWithMessage:@"Fill appropriate fields before logging in"];
    }
}

- (void)finishAuthorizationProcessWithSuccess:(BOOL)success {
    NSString * message;
    if (success) {
        message = @"You've been successfully logged in";
    }
    else {
        message = @"Login failed, please try again later";
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];

    [alert show];
    [self stopActivity];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self close];
}

#pragma mark - Keyboard handling

- (void)closeKeyboard {
    [self.emailTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.ssoEmailTextField resignFirstResponder];
}

#pragma mark - Activity indicator

- (void)startActivity {
    self.activityIndicator.hidden = NO;
    [self.activityIndicator startAnimating];
}

- (void)stopActivity {
    self.activityIndicator.hidden = YES;
    [self.activityIndicator stopAnimating];
}

#pragma mark - BreezySDK Delegate

- (void)didLogin {
    NSLog(@"did login");
}

@end
